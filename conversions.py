from math import pi
def area(radius):
    return pi * radius**2

def volume(area, length):
    return area * length
    
def radius(diameter):
    return diameter / 2

def mass(volume, density):
    return volume * density