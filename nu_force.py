import settings as conf
import thermodynamics as td
from conversions import radius, area, volume, mass

PRECISION = 2

def par_pressure(n_moles, temp, volume):
    GAS_CONSTANT = 8.3144621 #  m3 Pa /K /mol

    """ Partial pressure of a gas based on ideal gas law

        PV = nRT
    """
    return (n_moles * GAS_CONSTANT * temp) / volume

def n_moles(mass, mol_wgt):
    return  mass * 1e3 / mol_wgt

# Init components
capsule = conf.PELLET_A
body = conf.BODY_A

print('\n')
gunvol = volume(area(radius(body.diameter)), body.length)
print(f'The whole system has {round(gunvol * 1e3, PRECISION)} liters of space.')

# Calculate mass of the fluid in pellet
volume_pellet = volume(area(radius(capsule.diameter)), capsule.length)
mass_pellet = mass(volume_pellet, capsule.material.density)

print(f'The mass of the fuel fluid is {round(mass_pellet * 1e3,PRECISION)} g' \
    f' at {round(volume_pellet * 1e6, PRECISION)} ml.')

# Calculate specific energy assuming instantly opening to vacuum
spec_energy = td.specific_energy_phase(capsule.material.bp_vacuum, 
                                       capsule.state.temperature,
                                       capsule.material.specific_heat)

therm_energy = spec_energy * mass_pellet

hotmass= td.mass_heated(therm_energy, capsule.material.sp_latent_heat)

print(f'{round(therm_energy, PRECISION)} Joules of energy vaporizes ' \
      f'{round(hotmass * 1e3, PRECISION)} g of fluid on opening into partial vacuum.')

pressure = par_pressure(n_moles(hotmass, capsule.material.mol_weight),
                        body.state.temperature,
                        gunvol)

print(f'Pressure rises to {round(pressure, PRECISION)} Pascal'\
      f'({round(pressure / 1e5, PRECISION)} Atm).')

print(f'\033[1m{round(pressure * body.length * area(radius(body.diameter)), PRECISION)} \033[0m'\
     f' Joules of potential energy in the system.')
