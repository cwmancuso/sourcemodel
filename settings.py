from config import Unit, Material, Condition

#=====================================================================
# CONDITIONS
STP    = Condition(pressure = 1e5,          temperature = 20 + 273.15) # 20 Degree C Day
WARM   = Condition(pressure = STP.pressure, temperature = 50 + 273.15)
VACUUM = Condition(pressure = 1e3,          temperature = STP.temperature)

#=====================================================================
# MATERIALS
D_WATER  = Material(name           = 'Distilled Water', 
                    density        = 1000.0,    # kg/m3
                    specific_heat  = 4184.0,    # J/kg K
                    sp_latent_heat = 2270000.0, # J/kg
                    mol_weight     = 18.02,     # g/mol
                    bp_vacuum      = 280.0      # K, 7 degrees C
                   )
ETHANOL  = Material(name           = 'Ethanol', 
                    density        = 789.0,     # kg/m3
                    specific_heat  = 2460.0,    # J/kg K
                    sp_latent_heat = 846000.0,  # J/kg
                    mol_weight     = 46.07      #g/mol
                   )
ALUMINUM = Material(name = 'Aluminum',          density = 2700.)
PVC      = Material(name = 'Polyvinylchloride', density = 1300.)
AIR      = Material(name = 'Air',               density = 1225.)

#=====================================================================
# UNITS
BODY_A = Unit(  thickness = 0.01,    # m
                material  = AIR,
                diameter  = .1,      # m
                length    = 0.6,     # m
                state     = VACUUM
             )

MASS_A = Unit( material = PVC,
               length   = .1,
               diameter = BODY_A.diameter
             )

PELLET_A = Unit(diameter = .07, length = .03, material = D_WATER, state = STP)
PELLET_B = Unit(diameter = .07, length = .03, material = D_WATER, state = WARM)

# ====================================================================
