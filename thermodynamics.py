def specific_energy_phase(bp, temp, sp_heat):
    """ Specific energy available at PT conditions
    """
    del_t = temp - bp       # temperature above bp @ pressure
    return del_t * sp_heat  # Joules/kg

def mass_heated(energy, latent_heat):
    """ Mass of liquid converted to gas
    """
    return energy / latent_heat
