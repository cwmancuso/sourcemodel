from typing import NamedTuple

class Condition(NamedTuple):
    pressure:      float = 1e4, # Pa
    temperature:   float = 1e5,  # K

class Material(NamedTuple):
    name: str ='',
    density: float = 1.0,         # kg/m3
    specific_heat:  float = 1.0,  # KJ/(kg K)
    sp_latent_heat: float = 1.0,  # kJ/kg
    expansion_rat:  float = 1600, # times
    mol_weight:     float = 1.,   # g/mol
    bp_vacuum:      float = 0.,   # K, boiling point @ vacuum

class Unit(NamedTuple):
    length:   float = 1.0,      # m
    diameter: float = 0.05,     # m
    thickness:float = .1,       # m
    material: Material = None,
    state:    Condition = None,


    





